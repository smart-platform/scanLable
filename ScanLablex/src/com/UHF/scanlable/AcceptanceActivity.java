package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.model.EmployBean;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;


public class AcceptanceActivity extends ScanActivity implements OnClickListener{
	//确认单据
	private Button checkBill_bt;
	//物料复核
	private Button checkMaterial_bt;
	//复核完成
	private Button acceptance_bt;
//	private Button QRcodeBubmit;
	//单据号
	private String acceptanceCode;
	
	private ListView listView;
	private AcceptanceAdapter myAdapter;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	/**
	 * EPC号，物料流水号
	 */
	private Map<Object,Object> acceptanceData=new HashMap<Object,Object>();
	/**
	 * 物料流水号,复核状态
	 */
	private LinkedHashMap<Object,Object> acceptanceCheckData=new LinkedHashMap<Object,Object>();
	private Intent intent=null;
	private String ip;
	private Handler mHandler;
	private boolean pass=false;
	private ImageView setUrlBt;
	private int QR_requestCode=2;
	//标题
	private TextView cp_title;
	private String lastBt="0";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.acceptance);
		checkBill_bt = (Button)findViewById(R.id.checkBill_bt);
		checkBill_bt.setOnClickListener(this);
		checkMaterial_bt = (Button)findViewById(R.id.checkMaterial_bt);
		checkMaterial_bt.setOnClickListener(this);
		acceptance_bt = (Button)findViewById(R.id.acceptance_bt);
		acceptance_bt.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.acceptanceList);//
		acceptanceCode=getIntent().getStringExtra("acceptanceCode");
		cp_title = (TextView)findViewById(R.id.cp_title);//
		cp_title.setText("接收单据号:"+acceptanceCode);
//		QRcodeBubmit = (Button)findViewById(R.id.QRcode_button);
//		QRcodeBubmit.setOnClickListener(this);
		intent = new Intent();
			mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				String s=(String) msg.obj;
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if("-1".equals(s)){
						Toast.makeText(AcceptanceActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
					}else{
						JSONArray objList;
						try {
		    				JSONObject ss= new JSONObject(s);
		    				String operType=ss.getString("operType");
							objList = new JSONArray(ss.getString("list"));
							String types="";
							for (int i = 0; i< objList.length(); i++) {
				                JSONObject obj = objList.getJSONObject(i);
				                //循环遍历，依次取出JSONObject对象
				                //用getInt和getString方法取出对应键值
				                types=obj.getString("state");
			    				acceptanceData.put(obj.get("scanCode"),obj.getString("packageInventoryNum"));
			    				acceptanceCheckData.put(obj.getString("packageInventoryNum"), types);
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					if(myAdapter == null){
						myAdapter = new AcceptanceAdapter(AcceptanceActivity.this, acceptanceCheckData);
						listView.setAdapter(myAdapter);
					}else{
						myAdapter.setData(acceptanceCheckData) ;
					}
					myAdapter.notifyDataSetChanged();
			        pass=false;
					break;
				case 1:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					//Toast.makeText(AcceptanceActivity.this,data.keySet().toString(),Toast.LENGTH_SHORT).show();
					if(data==null||data.isEmpty()){
						break;
					}
					checkAcceptance(data);
					if(myAdapter == null){
						myAdapter = new AcceptanceAdapter(AcceptanceActivity.this, acceptanceCheckData);
						listView.setAdapter(myAdapter);
					}else{
						myAdapter.setData(acceptanceCheckData) ;
					}
					myAdapter.notifyDataSetChanged();
	//				Toast.makeText(AcceptanceActivity.this,new ArrayList(data.keySet()).size(),Toast.LENGTH_SHORT).show();
					break;
				case 2:
					if(s==null||"1".equals(s)){
						Toast.makeText(AcceptanceActivity.this,acceptanceCode+"单据复核失败！",Toast.LENGTH_SHORT).show();
					}else{
						try {
							String re="";
							JSONObject ss= new JSONObject(s);
							String operType=ss.getString("operType");
							String result=ss.getString("result");
							if("0".equals(result)){
								Toast.makeText(AcceptanceActivity.this,acceptanceCode+"单据复核完成！",Toast.LENGTH_SHORT).show();
								onBackPressed();
							}else{
								Toast.makeText(AcceptanceActivity.this,acceptanceCode+"单据复核失败！",Toast.LENGTH_SHORT).show();
							}
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
	//				Toast.makeText(AcceptanceActivity.this,new ArrayList(data.keySet()).size(),Toast.LENGTH_SHORT).show();
					break;
				default:
					break;
				}
				
			}
		};
	refreshList();
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, AcceptanceListActivity.class);  
        startActivity(intent);
        finish();
	}
	private boolean refreshList(){
		data.clear();
		acceptanceCheckData.clear();
		acceptanceCheckData.clear();
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
//		m.put("goodsName", goodsName);//货物名称
//		m.put("goodsCode", goodsCode);//货物编号
//		m.put("demandCount", demandCount);//需求量
//		m.put("assignCount", assignCount);//实发量
		m.put("acceptanceCode", acceptanceCode);//物料单据id
//		m.put("extendingMaterialId", extendingMaterialId);//物料单据id
		m.put("user_id", UfhData.getEmployBean().getId());
		checkDataFromServer.setOperType("17");//物料出库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(mHandler);
		checkDataFromServer.setWhat(0);
		return checkDataFromServer.checkData();
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
//		Toast.makeText(ProductOutActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		checkAcceptance(data);
		if(myAdapter == null){
			myAdapter = new AcceptanceAdapter(AcceptanceActivity.this, acceptanceCheckData);
			listView.setAdapter(myAdapter);
		}else{
			myAdapter.setData(acceptanceCheckData) ;
		}
		myAdapter.notifyDataSetChanged();
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.checkBill_bt:
			refreshList();
			break;
		case R.id.checkMaterial_bt://复核物料（盘询）
			if("0".equals(lastBt)){
				lastBt="1";
				data.clear();
				checkMaterial_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"0001");
			}else{
				lastBt="0";
				checkMaterial_bt.setText(R.string.scan);
				closeRFIDScan();
			}
			break;
		case R.id.acceptance_bt://完成复核
			if("0".equals(lastBt)){
				if("".equals(acceptanceCode)){
					Toast.makeText(this, R.string.noDocumentNo, Toast.LENGTH_LONG).show();
					return;
				}
				for(Object key:acceptanceCheckData.keySet()){  
					if(acceptanceCheckData.get(key).equals("待复核")){
						Toast.makeText(this, "该单据的物料还未全部复核，不能点击完成", Toast.LENGTH_LONG).show();
						return;
					}
				}
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				Map m=new HashMap();
//				m.put("goodsName", goodsName);//货物名称
//				m.put("goodsCode", goodsCode);//货物编号
//				m.put("demandCount", demandCount);//需求量
//				m.put("assignCount", assignCount);//实发量
				m.put("acceptanceCode", acceptanceCode);//物料单据id
//				m.put("extendingMaterialId", extendingMaterialId);//物料单据id
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("18");//物料出库待复核单据
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(mHandler);
				checkDataFromServer.setWhat(2);
				checkDataFromServer.checkData();
			}else{
				Toast.makeText(AcceptanceActivity.this,"请先停止扫码!",Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}
		return;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}
	private void checkAcceptance( Map<String,Integer> data){
		for(String key:data.keySet()){  
			if(acceptanceCheckData.get(acceptanceData.get(key))!=null){
				acceptanceCheckData.put(acceptanceData.get(key), "已复核");
			}
		}
	}
}
