package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.util.CheckDataFromServer;

public class MaterialInfoActivity extends Activity implements OnClickListener{

	Button button_write;

	EditText et_packageInventoryNumx0;
	EditText et_materialCodex0;
	EditText et_namex0;
	EditText et_specificationx0;
	EditText et_producersx0;
	EditText et_supplierx0;
	EditText et_fromBatchNox0;
	TextView et_producersx;
	TextView et_supplierx;
	TextView et_fromBatchNox;
	EditText et_batchNumx0;
	EditText et_counts0;
	EditText et_statusx0;
	EditText et_positionx0;

	
	Button button_back;
	/**
	 * 流水号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	private CheckBoxAdapter checkAdapter;
	private Intent intent;
	private String scanCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.material_info);
		scanCode= getIntent().getStringExtra("scanCode");
		intent = new Intent(this,ProductIncomingActivity.class);

		et_packageInventoryNumx0=(EditText)findViewById(R.id.et_packageInventoryNumx0);
		et_materialCodex0=(EditText)findViewById(R.id.et_materialCodex0);
		et_namex0=(EditText)findViewById(R.id.et_namex0);
		et_specificationx0=(EditText)findViewById(R.id.et_specificationx0);
		et_producersx0=(EditText)findViewById(R.id.et_producersx0);
		et_supplierx0=(EditText)findViewById(R.id.et_supplierx0);
		et_fromBatchNox0=(EditText)findViewById(R.id.et_fromBatchNox0);
		et_batchNumx0=(EditText)findViewById(R.id.et_batchNumx0);
		et_statusx0=(EditText)findViewById(R.id.et_statusx0);
		et_positionx0=(EditText)findViewById(R.id.et_positionx0);
		et_counts0=(EditText)findViewById(R.id.et_counts0);
		et_producersx=(TextView)findViewById(R.id.et_producersx);
		et_supplierx=(TextView)findViewById(R.id.et_supplierx);
		et_fromBatchNox=(TextView)findViewById(R.id.et_fromBatchNox);

		refreshList();
		
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("scanCode", scanCode);
		checkDataFromServer.setOperType("1");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==button_write.getId()){
			submitPackage();
		}else if(v.getId()==button_back.getId()){
			onBackPressed();
		}
	}
	public boolean submitPackage(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		String inventoryCount=et_counts0.getText().toString();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("inventoryCount", inventoryCount);//实际量
		m.put("scanCode", scanCode);
		checkDataFromServer.setOperType("1");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(1);
		checkDataFromServer.checkData();
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, QueryActivity.class); 
        startActivity(intent);
        finish();
	}
	
	


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialInfoActivity.this,"流水号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                JSONObject incomingMaterial= obj.getJSONObject("incomingMaterial");
    				                
    				                if(!(obj.isNull("packageInventoryNum")||"".equals(obj.get("packageInventoryNum")))){
    				                	et_packageInventoryNumx0.setText(obj.getString("packageInventoryNum"));
    				                }
    				                if(!(obj.isNull("materialNum")||"".equals(obj.get("materialNum")))){
    				                	et_materialCodex0.setText(obj.getString("materialNum"));
    				                }
    				                if(!(obj.isNull("materialName")||"".equals(obj.get("materialName")))){
    				                	et_namex0.setText(obj.getString("materialName"));
    				                }
    				                if(!(incomingMaterial.isNull("specification")||"".equals(incomingMaterial.get("specification")))){
    				                	et_specificationx0.setText(incomingMaterial.getString("specification"));
    				                }
    				                if(!(obj.isNull("supplierNameSC")||"".equals(obj.get("supplierNameSC")))){
    				                	et_producersx0.setText(obj.getString("supplierNameSC"));
    				                }
									if(!(obj.isNull("supplierNameJXS")||"".equals(obj.get("supplierNameJXS")))){
    				                	et_supplierx0.setText(obj.getString("supplierNameJXS"));
    				                }
    				                if(!(obj.isNull("supplierBatchNo")||"".equals(obj.get("supplierBatchNo")))){
    				                	et_fromBatchNox0.setText(obj.getString("supplierBatchNo"));
    				                }
    				                if(!(obj.isNull("inventoryNumber")||"".equals(obj.get("inventoryNumber")))){
    				                	et_batchNumx0.setText(obj.getString("inventoryNumber"));
    				                }
    				                if(!(obj.isNull("warehousePosCode")||"".equals(obj.get("warehousePosCode")))){
    				                	et_positionx0.setText(obj.getString("warehousePosCode"));
    				                }
    				                if(!(obj.isNull("weightCount")||"".equals(obj.get("weightCount")))){
    				                	et_counts0.setText(obj.getString("weightCount"));
    				                }
    				                if(!(obj.isNull("qualityState")||"".equals(obj.get("qualityState")))){
    				                	et_statusx0.setText(obj.getString("qualityState"));
    				                }
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialInfoActivity.this,"流水号有误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
