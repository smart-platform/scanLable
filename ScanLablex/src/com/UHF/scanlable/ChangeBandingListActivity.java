package com.UHF.scanlable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.util.CheckDataFromServer;

public class ChangeBandingListActivity extends Activity implements OnClickListener{
	
	Button relieve_bt;
	Button refresh;
	Button change_type_bt;
	Button incomingMButton;
	/**
	 * 单据号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	
	private Map allData=new HashMap();
	private AcceptanceAdapter myAdapter;
	private ListView listView;
	private Intent intent;

	//标题
	private TextView cp_title;
	/**
	 * 0:物料，1成品
	 */
	String change_type="0";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_position_banding);
		change_type=getIntent().getStringExtra("change_type");
		refresh = (Button)findViewById(R.id.banding_refresh);
		refresh.setOnClickListener(this);
		relieve_bt = (Button)findViewById(R.id.relieve_bt);
		relieve_bt.setOnClickListener(this);
		change_type_bt = (Button)findViewById(R.id.change_type_btx);
		change_type_bt.setOnClickListener(this);
		listView = (ListView)findViewById(R.id.check_position_listx);//
		intent = new Intent(this,ChangeBandingDetilActivity.class);
		//标题
		cp_title = (TextView)findViewById(R.id.cp_titlex);//
		if("0".equals(change_type)){
			cp_title.setText(R.string.changPositionTitle2);
		}else if("1".equals(change_type)){
			cp_title.setText(R.string.changPositionTitle4);
		}
		listView.setOnItemClickListener(new OnItemClickListener(){  
			  
            @Override  
            public void onItemClick(AdapterView<?> parent, View view,  
                    int position, long id) {  
        		String documentNo = myAdapter.getmList().get(position);
        		Map m=(Map) allData.get(documentNo);
        		intent.putExtra("viewId", m.get("viewId").toString());
        		intent.putExtra("change_type", change_type);//类型（0物料 1成品）
        		intent.putExtra("documentNo", documentNo);
//        		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
        		startActivity(intent);
        		finish();
            }  
        });  
		refreshList();
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==refresh.getId()){
			myAdapter = null;
			listData.clear();
			allData.clear();
			refreshList();
		}else if(v.getId()==relieve_bt.getId()){
			Intent intentx=new Intent(this,ChangePositionActivity.class);
			intentx.putExtra("change_type", change_type);//类型（0物料 1成品）
//    		intent.putExtra(MainActivity.EXTRA_EPC, myAdapter.getmList().get(position));
    		startActivity(intentx);
    		finish();
		}else if(v.getId()==change_type_bt.getId()){
			if("1".equals(change_type)){
				change_type="0";
				cp_title.setText(R.string.changPositionTitle2);
			}else if("0".equals(change_type)){
				change_type="1";
				cp_title.setText(R.string.changPositionTitle4);
			}
			refreshList();
		}
		
		
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("change_type", change_type);
		checkDataFromServer.setOperType("42");
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	
	
	

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, CircleActivity.class); 
        startActivity(intent);
        finish();
	}



	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(ChangeBandingListActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    								Map<Object,Object> m=new HashMap<Object,Object>();
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                types="解除绑定";
    				                try {
    				    				m.put("viewId", obj.getString("viewId"));//出库单据编号
    					                listData.put(obj.get("documentNo"), types);
    					                allData.put(obj.get("documentNo"), m);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(myAdapter == null){
    							myAdapter = new AcceptanceAdapter(ChangeBandingListActivity.this, listData);
    							listView.setAdapter(myAdapter);
    						}else{
    							myAdapter.setData(listData) ;
    						}
    						myAdapter.notifyDataSetChanged();
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(ChangeBandingListActivity.this,"无数据!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
