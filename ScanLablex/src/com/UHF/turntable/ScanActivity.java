package com.UHF.turntable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import com.UHF.model.AcceptanceAdapter;
import com.UHF.scanlable.UfhData;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 所有需要用到条码扫码都需要基础这个activity
 * @author major
 *
 */
public class ScanActivity extends Activity {
    
//    private EditText showScanResult;
//    private Button btn;
//    private Button mScan;
//    private Button mClose;
    private int type;
    private int outPut;
    
    private Vibrator mVibrator;
    private SoundPool soundpool = null;
    private int soundid;
    private String barcodeStr;//扫码得到的结果

    protected Timer timer;
	protected boolean Scanflag=false;
	protected boolean isCanceled = true;
	private static final int SCAN_INTERVAL = 10;
    
    private BroadcastReceiver mScanReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            // TODO Auto-generated method stub
        	if (intent.getAction().equalsIgnoreCase("df.scanservice.result")) {
	            barcodeStr = intent.getStringExtra("result");
	            //String barcodeStr = intent.getStringExtra("barcode_string");//直接获取字符串
	//            showScanResult.setText(barcodeStr);
	            showScanCode();
        	}
        }
    };
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        try {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
            mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		} catch (Exception e) {
			// TODO: handle exception
		}
    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
    	this.unregisterReceiver(mScanReceiver);
		Intent sendIntent = new Intent("df.scanservice.cancelapp");
		sendBroadcast(sendIntent);
        super.onPause();
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        Intent sendIntent = new Intent("df.scanservice.toapp");
		sendBroadcast(sendIntent);

		IntentFilter ItFilter = new IntentFilter();
		ItFilter.addAction("df.scanservice.result");
		this.registerReceiver(mScanReceiver, ItFilter);
        
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
    	if(keyCode==8){
          IntentFilter filter = new IntentFilter();
//          filter.addAction(SCAN_ACTION);
          registerReceiver(mScanReceiver, filter);
    	}
        return super.onKeyDown(keyCode, event);
    }
    
    /**
     * 扫码结果调用
     */
    protected void showScanCode() {
        // TODO Auto-generated method stub
    	
    }

	/**
	 * @return 扫码得到的结果
	 */
	public String getBarcodeStr() {
		return barcodeStr;
	}

	public void setBarcodeStr(String barcodeStr) {
		this.barcodeStr = barcodeStr;
	}
	protected void openRFIDScan(final Handler mHandler,Map<String, Integer> scanResult6c,final int what,final String scanType){
		/*try {
			UfhData.Set_sound(true);
			UfhData.SoundFlag=false;
			isCanceled = false;
			timer = new Timer();
		//	if (myAdapter != null) {
		//		UfhData.scanResult6c.clear();
		//		myAdapter.setData(new HashMap<String, Integer>());
		//		myAdapter.notifyDataSetChanged();
		//		Message m=new Message();
		//		m.what=0;
		//		mHandler.sendMessage(m);
		//	}
		//	checkMaterial_bt.setText("121212");
			scanResult6c=new HashMap<String, Integer>();
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					if(Scanflag)return;
						Scanflag=true;
					UfhData.read6c(scanType);
					Scanflag=false;
					Message m=new Message();
					m.what=what;
					mHandler.sendMessage(m);
				}
			}, 0, SCAN_INTERVAL);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
	}
	protected void closeRFIDScan(){
		/*try {
			isCanceled = true;
			if(timer != null){
				timer.cancel();
				timer = null;
//				checkMaterial_bt.setText(R.string.scan);
			}
			UfhData.Set_sound(false);
		} catch (Exception e) {
			// TODO: handle exception
		}*/
	}
	/**
	 * @param val
	 * @param title
	 * @return 补足20位并把title加在20位val前
	 */
	protected String checkHex(String val,String title){
		if(val.length()<20){
			String x="";
			for(int i=0;i<20-val.length();i++){
				x+="0";
			}
			val=title+x+val;
		}
		return val;
	}
	/**
	 * 输出newMap不在oldMap中的key
	 * @param oldMap
	 * @param newMap
	 * @return List
	 */
	protected List checkMapData(Map oldMap,Map newMap){
		List list=new ArrayList<String>();
		Set s=oldMap.keySet();
		for(Object key:newMap.keySet()){  
			if(!s.contains(key.toString())){
				list.add(key);
			}
		} 
		return list;
	}
	protected Object getKey(Map<Object,Object> map,String value){  
        Object key="";  
        for (Map.Entry entry : map.entrySet()) {  
            if(value.equals(entry.getValue())){  
                key=entry.getKey();  
            }  
        }  
        return key;  
    }  
	public String convertScanCode(String hex) {
		String mainCodeHex = hex.substring(4, 12);
		String mainCodeOct = Integer.valueOf(mainCodeHex, 16).toString();
		String serialCodeHex = hex.substring(12, 20);
		String serialCodeOct = Integer.valueOf(serialCodeHex, 16).toString();
		for(int i = serialCodeOct.length(); i < 9; i++) {
			serialCodeOct = "0" + serialCodeOct;
		}
		String checkCodeHex = hex.substring(20, 24);
		String checkCodeOct = Integer.valueOf(checkCodeHex, 16).toString();
		return mainCodeOct + serialCodeOct + checkCodeOct;
	}
}
