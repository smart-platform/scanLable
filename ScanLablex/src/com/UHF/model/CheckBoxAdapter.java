package com.UHF.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.UHF.R;
import com.UHF.scanlable.ProductIncomingActivity;
import com.UHF.scanlable.ProductListActivity;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;


public class CheckBoxAdapter extends BaseAdapter{
	
	private Context mContext;
	private List<String> mList;
	private LayoutInflater layoutInflater;
	private LinkedHashMap data=new LinkedHashMap();
	private String checkData="";
	private List<ItemView>  ivList=new ArrayList<ItemView>();
	private List checked = new ArrayList(); //ѡ�е���
	
	public CheckBoxAdapter(Context context, LinkedHashMap datas) {
		mContext = context;
		mList = new ArrayList(datas.keySet());
		data=new LinkedHashMap(datas);
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View view, ViewGroup viewParent) {
		// TODO Auto-generated method stub
		ItemView iv=null;
		if(view == null){
			iv = new ItemView();
			view = layoutInflater.inflate(R.layout.checkbox_list, null);
			iv.tvCode = (TextView)view.findViewById(R.id.acceptance_List);
			iv.checkBox = (CheckBox)view.findViewById(R.id.item_checkbox);
			iv.serialNum = (TextView)view.findViewById(R.id.serialNums);
			iv.tvNum = (TextView)view.findViewById(R.id.checkType);
			view.setTag(iv);
		}else{
			iv = (ItemView)view.getTag();
		}
		iv.tvCode.setText(mList.get(position));
		iv.tvNum.setText(data.get(mList.get(position)).toString());
		iv.serialNum.setText(String.valueOf(position+1));
		iv.checkBox.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
//				status.put(position, !status.get(position));
				CheckBox c=(CheckBox) v;
				if(!c.isChecked()){
					checkData="";
					checked.remove(mList.get(position));
				}else{
					checkData=mList.get(position);
					checked.add(checkData);
				}
				if(!"".equals(checkData)){
					for(ItemView i:ivList){
						if(!i.tvCode.getText().equals(checkData)){
							i.checkBox.setChecked(false);
						}
					}
				}	
				Toast.makeText(mContext, checkData, Toast.LENGTH_SHORT).show();
			}
		});
		ivList.add(iv);
		return view;
	}
	
	public class ItemView{
		CheckBox checkBox;
		TextView tvCode;
		TextView tvNum;
		TextView serialNum;
	}

	public List<String> getmList() {
		return mList;
	}

	public void setmList(List<String> mList) {
		this.mList = mList;
	}

	public Map getData() {
		return data;
	}

	public void setData(LinkedHashMap data) {
		mList = new ArrayList(data.keySet());
		this.data = data;
	}

	public String getCheckData() {
		return checkData;
	}

	public void setCheckData(String checkData) {
		this.checkData = checkData;
	}

	public List getChecked() {
		return checked;
	}

	public void setChecked(List checked) {
		this.checked = checked;
	}

}